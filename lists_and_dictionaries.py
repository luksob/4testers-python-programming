shopping_list = ["oranges", "water", "chicken", "potatoes", "washing liquid"]

print(shopping_list[-1])

shopping_list.append("lemons")

print(shopping_list[-1])

number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy)

first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)

animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 7,
    "male": True,
}
dog_age = animal["age"]
print("Dog age:", dog_age)

dog_name = animal["name"]
print("Dog name:", dog_name)

animal["age"] = 10
print(animal)

animal["owner"] = "Łukasz"
print(animal)

movies = ["Muminki", "Avatar", "Bad Boys", "Amelia", "Szczęki"]

movies.insert(0, "Transformers 3")
movies.remove("Avatar")
print(movies)

# Exercise - wydrukuj długość listy, pierwszy element, ostatni element a na końcu dodaj nowy email "cde@example.com"

emails = ["a@example.com", "b@example.com"]

print(len(emails))
print(emails[0])
print(emails[-1])
emails.append("cde@example.com")
print(emails)

friend = {
    "name": "Natalia",
    "age": 33,
    "hobby": ["spanie", "jedzenie"]
}

print(friend)



