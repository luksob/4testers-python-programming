import uuid


def print_each_student_name_capitalized(list_of_student_first_names):
    for first_name in list_of_student_first_names:
        print(first_name.capitalize())


def print_first_ten_integers_squared():  # Pętla for przebiegająca po liście
    first_ten_integers = [1, 2, 3, 4, 4, 5, 6, 7, 8, 9, 10]
    for integer in first_ten_integers:
        print(integer ** 2)


def print_first_ten_integers_squared():  # Pętla for przebiegająca po zakresie 1,31
    for integer in range(1, 31):
        print(integer ** 2)


def print_10_random_uuids(number_of_string):
    for i in range(number_of_string):
        print(str(uuid.uuid4()))


def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number + 1):
        if number % 7 == 0:
            print(number)


def print_temperature_in_celcius_and_fahrenheit(temperature_in_celcius):
    for temperature in temperature_in_celcius:
        temp_fahrenheit = temperature * 9 / 5 + 32
        print(f'Celcius:{temperature}, Fahrenheit: {temp_fahrenheit}')


if __name__ == '__main__':
    list_of_students = ["zosia", "gabrysia", "marEK", "tomek", "ALFRED"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_integers_squared()
    print_10_random_uuids(7)
    print_numbers_divisible_by_7(1, 30)
    temps_celcius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temperature_in_celcius_and_fahrenheit(temps_celcius)
