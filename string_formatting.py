my_name = "Lukasz"
favourite_movie = "Star Wars"
favourite_actor = "Denzel Washington"

"My name is NAME. My favourite movie is MOVE. My favourite actor is ACTOR"


# bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}"
# print(bio)

# Exercise 1 - Display welcome text using name and city of user
def display_welcome_text(name, city):
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city}")


display_welcome_text("michał", "Toruń")
display_welcome_text("beata", "Gdynia")

# Exercise 2 - Generate email in 4testers.pl domain using first and last name
def generate_email_from_user_firstname_and_lastname(first_name, last_name):
    return (f"{first_name.lower()}.{last_name.lower()}@4testers.pl")


print(generate_email_from_user_firstname_and_lastname("Janusz", "Nowak"))
print(generate_email_from_user_firstname_and_lastname("Barbara", "Kowalska"))
