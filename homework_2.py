def calculate_sum_of_numbers(list_with_numbers):
    return sum(list_with_numbers) / len(list_with_numbers)


temperatures_in_january = [-4, 1.0, 7.0, 2]
temperatures_in_february = [-13, -9, -3, 3]

print(f"January: {calculate_sum_of_numbers(temperatures_in_january)}")
print(f"February: {calculate_sum_of_numbers(temperatures_in_february)}")


def calculate_average_temperature(list_with_numbers):
    return sum(list_with_numbers) / len(list_with_numbers)


# def generate_random_data_for_email(email):


def print_player_description(gamer_dictionary):
    print(
        f"The player {gamer_dictionary['nick']} is of type {gamer_dictionary['type']} and has {gamer_dictionary['exp_points']}")


test = {
    "nick": "maestro_54",
    "type": "warrior",
    "exp_points": 3000
}

print_player_description(test)

list_with_addresses = [
    {
        "city": "Zyrardow",
        "street": "Izy Zielinskiej",
        "house_number": 22,
        "post_code": "96-300"
    },
    {
        "city": "Warszawa",
        "street": "Pruszkowska",
        "house_number": 14,
        "post_code": "02-119"
    },
    {
        "city": "Radom",
        "street": "Fasolowa",
        "house_number": 5,
        "post_code": "11-123"
    }]

print(list_with_addresses[-1]["post_code"])
print(list_with_addresses[1]["city"])
list_with_addresses[0]["street"] = "Gumisiowa"
print(list_with_addresses)


def check_if_temp_is_normal(temperature, pressure):
    if temperature == 0 & pressure == 1013:
        return True
    else:
        return False


first_example = (0, 1014)
# second example =
# third example =
# fourth example = (1,1014)

print(check_if_temp_is_normal(0, 1013))
print(check_if_temp_is_normal(1, 1014))
print(check_if_temp_is_normal(0, 1014))
print(check_if_temp_is_normal(1, 1014))


def check_what_grade_from_punctation(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >= 75:
        return 4
    elif number_of_points >= 50:
        return 3
    else:
        return 2

print(check_what_grade_from_punctation(91))
print(check_what_grade_from_punctation(90))
print(check_what_grade_from_punctation(89))
print(check_what_grade_from_punctation(76))
print(check_what_grade_from_punctation(75))
print(check_what_grade_from_punctation(74))
print(check_what_grade_from_punctation(50))
print(check_what_grade_from_punctation(49))