# Exercise 1
def calculate_number_square(number):
    return number ** 2


square_number = calculate_number_square(5)
print(square_number)

negative_number = calculate_number_square(-3)
print(negative_number)

negative_number = calculate_number_square(-3)
print(negative_number)


# Exercise 2
def calculate_cuboid_volume(a, b, c):
    return a * b * c


print(calculate_cuboid_volume(3, 5, 7))


# Exercise 3

def convert_celcius_to_fahrenheit(degrees):
    return 32 + 9 / 5 * degrees


converted_value_from_celcius_to_fahrenheit = convert_celcius_to_fahrenheit(20)

print(converted_value_from_celcius_to_fahrenheit)